package Presentacion;

	public class Hombre extends Persona {

		public Hombre(double peso, double altura, double edad) {
			super(peso, altura, edad);
		}
		
		public double TMB() {
			return 88.362 + (13.397 * getPeso()) + (4.799 * getAltura()) - (5.677 * getEdad());
		}

	}

