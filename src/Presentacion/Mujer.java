package Presentacion;

	public class Mujer extends Persona {

		public Mujer(double peso, double altura, double edad) {
			super(peso, altura, edad);
		}

		public double TMB() {
			return 447.593 + (9.247 * getPeso()) + (3.098 * getAltura()) - (4.33 * getEdad());
		}

		}

